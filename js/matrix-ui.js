var globalMutex = 0;

function stopAlgorithm() {
    // Reset mutex - algorithm should be stopped
    globalMutex = 0;
}

function goMatrixBySpiral() {
    var matrix, resultIndexes, matrixLength,
        maxInt = 100;
    var inputField = document.getElementById('matrix-size'),
        stopBtn = document.getElementById('stop-spiral');

    function IsNumeric(number) {
        // Check if input value is integer or not
        return (number - 0) == number && (''+number).trim().length > 0;
    }

    // Set mutex - algorithm was started
    // (used in function `colorMatrixByIndexes`)
    globalMutex = 1;

    // Show stop btn
    stopBtn.style.display = 'inline-block';

    // Remove table's rows
    clearMatrix();

    matrixLength = inputField.value;
    if (!IsNumeric(matrixLength)) {
        inputField.className += ' ';
        return false;
    }

    matrixLength = Math.floor(Number(matrixLength));
    // Generate matrix
    matrix = generateSquaredMatrix(matrixLength);

    // UI functions
    fillMatrix(matrix, maxInt);
    drawMatrix(matrix);

    // Run spiral algorithm
    resultIndexes = getSpiralIndexes(matrix);

    colorMatrixByIndexes(matrix, resultIndexes, 0);
}
