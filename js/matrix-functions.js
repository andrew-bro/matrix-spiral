
function generateSquaredMatrix(n) {
    /*
    Create new squared matrix for the given size
     */
    var matrix = new Array(n);

    for (var i = 0; i < n; i++) {
        matrix[i] = new Array(n);
    }

    return matrix
}

function fillMatrix(matrix, maxInt) {
    /*
    Fill given matrix by random integer values
    @param {array} matrix
    @param {number} maxInr
     */
    var matrixLength = matrix.length;
    for (var i = 0; i < matrixLength; i++) {
        for (var j = 0; j < matrixLength; j++) {
            matrix[i][j] = Math.floor(Math.random() * maxInt);
        }
    }
}

function getSpiralIndexes(matrix) {
    /*
    Для более понятного описания алгоритма составил текст на русском языке
    Получим список индексов элеметов матрицы для обхода по спирали.

    Порядок обхода будет восстанавливать от обратого, то есть с конца (снаружи),
    а затем просто "перевернем" полученный результат.
    Для квадратных матриц с четным N обходить можно всегда с правого нижнего
    угла, а для матрис с нечетным N - с левого верхнего

    Алгоритм построения индексов элементов простыми словами можно описать
    в следующим образом:
        Мы как бы "раздеваем" матрицу, снимая слой за слоем в цикле
        (cropOuterLayers).

        Убираем последнюю строку, затем первый столбец, затем первую строку и
        наконец последний столбец.
        Если у нас есть такая матрица
        1  2  3  4
        5  6  7  8
        9  10 11 12
        13 14 15 16
        То
        Получаем нижнюю строку:  16 15 14 13
        Затем первый столбец
        (за исключением первого
        и последнего элемента):  9 5
        Затем верхнюю строку:    1 2 3 4
        Затем последний столбец: 8 12
        После чего у нас остается матрица:
        6  7
        10 11
        Перехедоим на новую итерацию цикла.
        В результате мы получаем порядок обхода матрицы по спирали
        снаружи в центр. Чтобы начать обход из центра, нам достаточно
        перевернуть полученный результат

    В итоге функция возвращает массив вида:
    [[1,2], [1,1], [2, 1] ...] - упорядоченный массив индексов элементов
    матрицы, согласно которому можно обойти матрицу по спирали
    */
    var leftIndx = 0,
        rightIndx = matrix.length,
        resultIndexes = [];             // Container for result indexes

    var matrixLength = rightIndx;
    var isEvent = matrixLength % 2 == 0;

    // Declare containers for temporary results of cycle
    var bottomArr, topArr, leftArr, rightArr;

    function reinitializeTmpContainers() {
        /*
        Reinitialize temporary containers. Set them to empty arrays.
        */
        bottomArr = [];
        topArr = [];
        leftArr = [];
        rightArr = [];
    }

    function reverseLeftAndBottomTmpContainers() {
        /*
        Elements if left and bottom temporary containers should be reversed
        as was declared at algorithm docstring
        */
        bottomArr.reverse();
        leftArr.reverse();
    }

    function cropOuterLayers() {
        /*
        "Crop" bottom, left, top, right layers from matrix
         */
        for (var j = leftIndx; j < rightIndx; j++) {
            bottomArr.push([rightIndx - 1, j]);
            topArr.push([matrixLength - rightIndx, j]);

            if (rightIndx - j > 2) {
                leftArr.push([j + 1, leftIndx]);
                rightArr.push([j + 1, rightIndx - 1]);
            }
        }
    }

    function transferTmpResultIfEvent() {
        /*
        If N is event then we should start from bottom row
        */
        resultIndexes.push.apply(resultIndexes, bottomArr);
        resultIndexes.push.apply(resultIndexes, leftArr);
        resultIndexes.push.apply(resultIndexes, topArr);
        resultIndexes.push.apply(resultIndexes, rightArr);
    }

    function transferTmpResultIfNotEvent() {
        // If N isn't event then we should start from top row
        resultIndexes.push.apply(resultIndexes, topArr);
        resultIndexes.push.apply(resultIndexes, rightArr);
        resultIndexes.push.apply(resultIndexes, bottomArr);
        resultIndexes.push.apply(resultIndexes, leftArr);
    }

    // The main cycle of this algorithm
    while (rightIndx - leftIndx > 1) {
        // Reset temporary containers
        reinitializeTmpContainers();

        // Fill temp containers
        cropOuterLayers();

        // Reverse bottom and left rows
        reverseLeftAndBottomTmpContainers();

        // Copy indexes to result array
        isEvent ? transferTmpResultIfEvent() : transferTmpResultIfNotEvent();

        rightIndx -= 1;
        leftIndx += 1;
    }

    resultIndexes.reverse();

    if (!isEvent) {
        /*
        Insert first center element at the beginning of `resultIndexes`
        because it was ignored above
        */
        var centerIndx = (matrixLength - 1) / 2;
        resultIndexes.unshift([centerIndx, centerIndx]);
    }

    return resultIndexes;
}

function drawMatrix(matrix) {
    /*
    Draw matrix at the page (inside <table> tag)
     */
    var matrixLength = matrix.length;
    var table = document.getElementById('matrix');

    for (var i = 0; i < matrixLength; i++) {
        // Create row(<tr>) element
        var row = document.createElement('tr');

        // Create cell(<td>) and set appropriate value
        for (var j = 0; j < matrixLength; j++) {
            var cell = document.createElement('td');

            cell.setAttribute('id', i + '-' + j);
            cell.innerHTML = matrix[i][j];
            row.appendChild(cell);
        }
    table.appendChild(row);
    }
}

function clearMatrix(matrix) {
    /*
    Clear matrix rows at the page
     */
    var table = document.getElementById('matrix');
    table.innerHTML = '';
}

function colorMatrixByIndexes(matrix, resultIndexes, i) {
    /*
    Fill table's cells with colors step by step by spiral appropriate to order
    from `resultIndexes`
     */
    setTimeout(function () {
        var cell = document.getElementById(resultIndexes[i][0] + '-' + resultIndexes[i][1]);
        cell.style.backgroundColor = '#5cb85c';
        i++;
        if (i < resultIndexes.length) {
            if (globalMutex) {
                colorMatrixByIndexes(matrix, resultIndexes, i);
            }
        }
    }, 600);
}